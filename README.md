# Ventilificator

The Ventilificator is a conversion kit that turns an ordinary DC motor air pump (such as an air mattress pump that plugs into your car) into a ventilator (for emergency use only). Air mattresss inflators are very common and innexpensive and can usually provide a pressure of ~1PSI (70cmH2O), which lines up reasonably well with medical ventilator ranges (a medical ventillator would use a few tens of cmH2O at its highest settings).

**WARNING/DISCLAIMER:** This is not a medical device. In the event that the health care system runs out of FDA approved ventilators, this product could used to as a substitute. There are significant risks associated with using this device. Higher pressures can cause damage to lungs. The use of this conversion kit should be a last resort (or educational resource). Whenever possible, seek professional medical care. 

![](images/pcb_3d.PNG "3D rendering of PCB")

## What I need

With this COVID-19 situation, daycares are closed and I'm working full time from home with a 3-year-old in the middle of potty training. I don't have more time to devote to the project than hardware. I'd like to find volunteers to help me bring this to the next level and more importantly, I'd like someone to take over the project while I take on a hardware support (and software review) role.

* Someone to oversee the project 
  * Project management
  * Logistics
  * Manage GitLab permissions
* Hardware designer
  * Review schematic and layout
  * Cable drawings if applicable
* Software designer
  * STM32 firmware
* Mechanical designer
  * Design enclosure
* Funding to build prototype
  * I could start a GoFundMe
  * Making the purchase yourself works too

## Hardware capabilities/limitations

On the input side, a DC voltage is provided. This input power is used to power the board (on-board 3.3V buck converter) and power the load. The load (the DC pump) is PWM'd by the MCU (STM32F0).  

There is a pressure sensor (by Honeywell) to measure the air pressure. A tube should be connected near the "point of interest" (i.e. the patient, but connecting near the pump may be acceptable). This sensor allows us to run a control loop.

The device has three potentiometers for setpoints:

1. Upper pressure setpoint (cmH2O)
2. Lower pressure setpoint (cmH2O)
3. Rate (breaths per minute)

The device has a 3-digit 7-sement display to provide live measurements of the pressure (in cmH2O). When adjusting one of the setpoints (potentiometers), the LED next to the potetiometer is lit and the 3-digit display shows the value of the setpoint.

Input voltage was initially intended for 12V, but it should work fine anywhere between 5V and 32V. 

The power switch should be able to carry several amps continuously. At ~10A we'd start getting pretty hot. 

The pressure sensor can sense 0 to ~70cmH2O (above atmospheric pressure).

There is a buzzer that can indicate an error condition:
* Pressure is too high
* Pressure is not reaching setpoint
  * This could mean the feedback tube is disconnected, so please don't drive the pump too hard and make the patient explode
* Communication error (e.g. talking to sensor)
* Other errors

## Future 

An AC version might be created if there is interest. The PWM'd MOSFET would be replaced with a zero-cross TRIAC control or similar. Not sure if it would take separate DC for on board power or if it would have an on-board AC-DC converter.

## Licensing

Click [here](LICENSE) for licensing information. 

## Other

Connect with me on LinkedIn [here](https://www.linkedin.com/in/alexandre-leblanc).

Check out [Johnny Lee](https://github.com/jcl5m1)'s [Low-Cost Open-Source Ventilator-ish Device or PAPR](https://github.com/jcl5m1/ventilator), which repurposes a CPAP machine! Great work Johnny.
